defmodule Parko.ZoneDeterminer do
    def calc_dist(lat1, lng1) do
        lat2 = 58.381226
        lng2 = 26.719588
        earthRadius = 6371000; #meters
        dLat =(lat2-lat1)* :math.pi()/ 180;
        dLng = (lng2-lng1)*:math.pi()/ 180; 
        a = :math.sin(dLat/2) * :math.sin(dLat/2) +
            :math.cos((lat1)*:math.pi()/ 180) * :math.cos((lat2)*:math.pi()/ 180) *
            :math.sin(dLng/2) * :math.sin(dLng/2);
        c = 2 *:math.atan2(:math.sqrt(a), :math.sqrt(1-a));
        dist = earthRadius * c;

        dist
    end  
end