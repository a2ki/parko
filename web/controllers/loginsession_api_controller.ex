defmodule Parko.LoginsessionAPIController do
    use Parko.Web, :controller
    alias Parko.{Repo,User,Authentication}
  
    def create(conn, %{"username" => username, "password" => password}) do
      user = Repo.get_by(User, username: username)  
      
      IO.inspect Authentication.check_credentials(conn, user, password)  
      case Authentication.check_credentials(conn, user, password) do
      {:ok, _} ->
          conn2 = login(conn,user)
          #IO.inspect Guardian.encode_and_sign(user, :token) 
          IO.inspect "claim"
          {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user, :token)
          #IO.inspect conn|> Guardian.Plug.sign_in(user)

          IO.inspect  Guardian.decode_and_verify(jwt)

          conn2
          |> put_flash(:info, "You’re now logged in!")
          |> redirect(to: page_path(conn, :index))
          
      {:error, _} ->
          conn
          |> put_status(400)
          |> json(%{message: "Bad credentials"})
      end
    end
  
    def delete(conn, _params) do
      {:ok, claims} = Guardian.Plug.claims(conn)
      conn
      |> Guardian.Plug.current_token
      |> Guardian.revoke!(claims)
  
      conn
      |> put_status(200)
      |> json(%{msg: "Good bye"})
    end
  
    def unauthenticated(conn, _params) do
      conn
      |> put_status(403)
      |> json(%{msg: "You are not logged in"})
    end

    defp login(conn, user) do
      conn
      |> Guardian.Plug.sign_in(user)
    end
  end  