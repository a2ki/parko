defmodule Parko.PageController do
  use Parko.Web, :controller
  alias Parko.LoginsessionAPIController 

  def index(conn, _params) do
    IO.inspect Guardian.Plug.current_resource(conn)
    render conn, "index.html"
  end
end
