defmodule Parko.SessionController do
    use Parko.Web, :controller
  
    def new(conn, _params) do
      render conn, "new.html"
    end

    def create(conn, %{"session" => %{"username" => username, "password" => password}}) do
        case Parko.Authentication.check_credentials(conn, username, password, repo: Parko.Repo) do
          {:ok, conn} ->
            conn
            |> put_flash(:info, "Welcome #{username}")
            |> redirect(to: page_path(conn, :index))
          {:error, _reason, conn} ->
            conn
            |> put_flash(:error, "Bad credentials")
            |> render("new.html")
        end    
    end

    def delete(conn, _params) do
      IO.inspect "delete"
        conn
        |> Parko.Authentication.logout()
        |> redirect(to: page_path(conn, :index))
    end

  end