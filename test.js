<template>
  <div>
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
         <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
       <a class="navbar-brand" href="#/">Parko</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Park Me</a></li>
      <!--  <li><a href="#">About</a></li>
        <li><a href="#">Gallery</a></li>
        <li><a href="#">Contact</a></li>-->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li v-if="!username" ><a href="#/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
         <li v-if="username" v-on:click="logout()"><a ><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

    <div>
      <li>Welcome, {{username}} </li> 
    </div>

    <div class="jumbotron" style= "background-image: url(/images/jumbo-back.jpg)">
      <div class="container text-center">
        <div class="coloring" style="color: rgb(226, 217, 217)">
            <h1>Parko</h1>      
             <p>Find the best parking place in Tartu</p>
        </div>
     </div>
   </div>
   <div>
   <div >
    <label class="control-label col-sm-3" for="destination_address">Destination address:</label>
    <div class="col-sm-9">
       <input type="text" class="form-control" id="destination_address" v-model="destination_address">
      <button class="btn btn-default" v-on:click="searchRequest">Search</button>
    </div>
   </div>
    
    <div>
       <div class="container" v-if="spaces_in_range">
        <div v-if="spaces_in_range.length > 0">
         <div  class="col-12 col-sm-12 col-lg-12 table-responsive">
           <h3 style="margin-left: 30%"> Available parking spaces </h3>
          <table class="table" style="border-collapse: collapse;" cellspacing="0" width="auto">
            <thead>
              <tr>
                <th>Address</th>
                <th>Zone</th>
                <th>Estimated hourly price</th>
                <th>Estimated realtime price</th>
                <th>Available spaces</th>
                <th>Distance to destination</th>
                <th>Map View</th>
                <th>Pay hourly</th>
                <th>Pay realtime </th>
              </tr>
            </thead>
            <tbody>
              <tr class = "available_park" v-for ="(item, index) in spaces_in_range">
                <td>{{item.address}}</td>
                <td>{{item.zone}}</td>
                <td>{{item.est_price_hourly}} €</td>
                <td>{{item.est_price_real_time}} €</td>
                <td>{{item.available_space}}</td>
                <td>{{item.distance}} m</td>
                <td><button type="button" class="btn btn-default btn-sm" v-on:click="focusAndHighlight(index)">On map</button></td>
                <td><button :disabled = "!end_time || !start_time" type="button" class="btn btn-default btn-sm" v-on:click="submitBookingRequest(index, 'HOURLY')">Pay hourly</button></td>
                <td><button :disabled = "!end_time || !start_time" type="button" class="btn btn-default btn-sm" v-on:click="submitBookingRequest(index, 'REALTIME')">Pay realtime</button></td>
              </tr>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    <div v-else>
          <span>No available parking spaces where found</span>
    </div>
    </div>
    <div class="container" v-if="showModal">
        <div class="modal-mask">
          <div class="modal-wrapper">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" @click="showModal=false">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">Very nice parking place, just for you :D</h4>
                </div>
                <div class="modal-body">
                  <div class="table-responsive panel">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td class="text-success"><i class="fa fa-user"></i> Address</td>
                          <td>{{spaces_in_range[modal_id].address}}</td>
                        </tr>
                        <tr>
                          <td class="text-success"><i class="fa fa-list-ol"></i> Price for hourly payment</td>
                          <td>
                            <span v-if = "spaces_in_range[modal_id].est_price_hourly">{{spaces_in_range[modal_id].est_price_hourly}} €</span>
                            <span v-if = "!spaces_in_range[modal_id].est_price_hourly">Info not available</span>
                          </td>
                        </tr>
                        <tr>
                          <td class="text-success"><i class="fa fa-book"></i> Price for realtime payment</td>
                          <td>
                            <span v-if = "spaces_in_range[modal_id].est_price_real_time">{{spaces_in_range[modal_id].est_price_real_time}} €</span>
                            <span v-if = "!spaces_in_range[modal_id].est_price_real_time">Info not available</span>
                          </td>  
                        </tr>
                        <tr>
                          <td class="text-success"><i class="fa fa-book"></i> Zone</td>
                          <td>{{spaces_in_range[modal_id].zone}}</td>
                        </tr>
                        <tr>
                          <td class="text-success"><i class="fa fa-book"></i> Available places</td>
                          <td>{{spaces_in_range[modal_id].available_space}}</td>
                        </tr>
                        <tr>
                          <td class="text-success"><i class="fa fa-book"></i> Distance to destination</td>
                          <td>{{spaces_in_range[modal_id].distance}} m</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" @click="showModal=false">Cancel</button>
                    <button type="button" :disabled = "!end_time || !start_time" class="btn btn-default" v-on:click="submitBookingRequest(modal_id, 'HOURLY')">Pay hourly</button>
                    <button type="button" :disabled = "!end_time || !start_time" class="btn btn-default" v-on:click="submitBookingRequest(modal_id, 'REALTIME')">Pay realtime</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container" v-if="showMessage">
        <div class="modal-mask2">
          <div class="modal-wrapper2">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" @click="showMessage=false">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">{{title}}</h4>
                </div>
                <div class="modal-body">
                  {{message}}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" @click="showMessage=false">OK</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="map" style="width:100%;height:300px"></div>
    </div>  
    <footer style="background-color: #f2f2f2;">
      <div class="container" style="background-color: #f2f2f2;">
        <h2>See more parking spaces </h2>
        <p>Click on the images to see more.</p>
     
      <div class="row" style="height: 100%">
        <div class="col-md-4">
            <div class="thumbnail">
              <a href="/images/Parkimismaja.jpg" target="_blank">
                <img src="/images/Parkimismaja.jpg" alt="Lights" style="width:100%">
                <div class="caption">
                  <p>There is a nice parking space at Parkimismaja.</p>
                </div>
              </a>
            </div>
          </div>
        <div class="col-md-4">
            <div class="thumbnail" >
              <a href="/images/EuroPark Ülikooli kliinikum.jpg" target="_blank">
                <img src="/images/EuroPark Ülikooli kliinikum.jpg" alt="Nature" style="width:100%">
                <div class="caption">
                  <p>Another nice parking space at EuroPark Ülikooli kliinikum.</p>
                </div>
              </a>
            </div>
        </div>

          <div class="col-md-4">
            <div class="thumbnail">
              <a href="/images/Parking place.jpg" target="_blank">
                <img src="/images/Parking place.jpg" alt="Fjords" style="width:100%">
                <div class="caption">
                  <p>Parking place is not left out.</p>
                </div>
              </a>
            </div>
          </div>
      </div>   
      </div>
    </footer>
  </div>
</template>

<script>
  import axios from 'axios';
  import auth from "./auth";
  export default {
    data: function() {
      return {
        destination_address: "Raatuse 22",
        start_time: "",
        end_time: "",
        showModal: false,
        showMessage: false,
        spaces_in_range: null,
        message: "",
        title: "",
        username:null,
        modal_id: null,
        markers: [],
        polygons: []
      }
    },
    methods: {
      submitBookingRequest: function(marker_index, payment_method) {
        var that = this;
        var park_id = this.spaces_in_range[marker_index].park_id
        var start = this.start_time
        var end = this.end_time 
        axios.post("api/parking_book", {park_id: park_id, payment_method: payment_method, start: start, end: end})
          .then(function (response) { 
            that.showMessage = true;
            that.title = "Confirmation";
            that.message = response['data']['msg'];
            const spaces = that.spaces_in_range[marker_index].available_space;
            that.spaces_in_range[marker_index].available_space = spaces - 1;
          })
          .catch(function (error) {
            that.title = "Something went wrong. Please try again.";
            that.message = error.response.data.msg;
          }); 

          //here you can find parking place id and make axios post to controller to deal with the booking
        this.showModal = false;
      },
      searchRequest: function() {
        var that = this;
        var my_map = this.map
        var address = this.destination_address;
        var start = this.start_time
        var end = this.end_time
        this.geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == 'OK') {
            this.map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: results[0].geometry.location});
            var marker = new google.maps.Marker({
              map: this.map,
              position: results[0].geometry.location
            });

            var long = results[0].geometry.location.lng();
            var lat = results[0].geometry.location.lat();
                    
            axios.post("api/parking_find", {lng: long, lat: lat, start:start, end:end})
              .then(function (response) { 
                that.spaces_in_range = response['data']['spaces_in_range']; 
                //console.log(response['data']['spaces_in_range'])
                that.draw_map();
              })
              .catch(function (error) {
                console.log(error);
              });          
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      },
      focusAndHighlight(index){
        for(var i in this.polygons){
          const color = this.spaces_in_range[i].zone === 'A' ? '#3385ff' : '#33ff33';
          this.polygons[i].setOptions({
            strokeColor: color,
            fillColor: color,
          });
        }
        this.polygons[index].setOptions({
          strokeColor: '#ff0000',
          fillColor: '#ff0000',
        });
        map.setCenter(this.markers[index].getPosition());
      },
      showModalFn(index){
        this.modal_id = index;
        this.showModal = true;
      },
      draw_map:function(){
        var that = this;

        for (var i in this.spaces_in_range ) {
          var item = this.spaces_in_range[i];

          var loc = { lat: item.lat, lng: item.long}; //info marker location
          var whichIcon = null; 

          //observer object to array of maps
          const points =  [];
          for(var u in item.points) {
            const longitude = item.points[u].long;
            const latitude = item.points[u].lat;
            points.push({lat: latitude, lng: longitude});
          }
                
          var color = null;
              
          if(item['zone'] === 'A') {
            color = '#3385ff';
            whichIcon = '/images/parking_iconA.png';
          } else {
            color = '#33ff33';
            whichIcon = '/images/parking_iconB.png';
          }
          var marker = new google.maps.Marker({
            position: loc,
            icon: whichIcon,
            map: map
          });
                
          marker.metadata = {
            'type': 'point', 
            'id': that.markers.length - 1
          };
          that.markers.push(marker);
          marker.addListener('click', (function(index) {
            return function() {
              that.showModalFn(index);
            };
          })(that.markers.length - 1));
                
          var poly = new google.maps.Polygon({
            paths: points,
            strokeColor: color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.35
          });

          poly.setMap(map);
          that.polygons.push(poly);
        }
      },
      logout: function () {
        this.username = null;
        auth.logout(this, {headers: auth.getAuthHeader()});
    }
    },
    mounted: function () {
      if(auth.user.username && auth.user.username.length>0){
       this.username = auth.user.username;
       }
      navigator.geolocation.getCurrentPosition(position => {
        let loc = {lat: position.coords.latitude, lng: position.coords.longitude};
        this.geocoder = new google.maps.Geocoder;
        this.geocoder.geocode({location: loc}, (results, status) => {
          if (status === "OK" && results[0])
            this.destination_address = results[0].formatted_address;
        });
        this.map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: loc});
        new google.maps.Marker({position: loc, map: this.map, title: "Destination address"});
      });
    
    }

}
</script>

<style>


</style>
