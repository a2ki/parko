defmodule Parko.PointTest do
  use Parko.ModelCase

  alias Parko.Point

  @valid_attrs %{lat: 120.5, lng: 120.5}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Point.changeset(%Point{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Point.changeset(%Point{}, @invalid_attrs)
    refute changeset.valid?
  end
end
